import React from 'react'
import { connect } from 'react-redux'

const cmd = require('../commands')

class Search extends React.Component {

  constructor(props) {
    super(props)
    this.state = { 
      mode: 0,
      nm: '',
      nums: ['','','','','','','','','','']
    }
  }

  render() {
    switch(this.state.mode){
      case 0:
        return <div className="find lft">
          <button onClick={() => this.setState({ mode:1 })} type="button" className="btn btn-outline-primary btn-sm lft">
            <i className="icon-find"></i>
          </button>
        </div>  

      case 1:
        return <div className="find lft">
          <button onClick={() => this.setState({ mode:0 })} type="button" className="btn btn-outline-danger btn-sm lft">
            <i className="icon-cancel"></i>
          </button>
          <button onClick={() => this.setState({ mode:2 })} type="button" className="btn btn-outline-primary btn-sm lft">
            <i className="icon-find"></i>АБВ...
          </button>
          <button onClick={() => this.setState({ mode:3 })} type="button" className="btn btn-outline-primary btn-sm lft">
            <i className="icon-find"></i>123...
          </button>
        </div>  

      case 2:
        return <div className="find lft">
          <button onClick={() => this.setState({ mode:0 })} type="button" className="btn btn-outline-danger btn-sm lft">
            <i className="icon-cancel"></i>
          </button>
          <input 
            value={this.state.nm} 
            onChange={
              (e) => {
                if(/^[А-Яа-яІіЄєЇї]+$/.test(e.target.value))
                  this.setState({nm: e.target.value})
              }
            } 
            className="form-control form-control-sm lft" type="text" placeholder="имя ..." 
          />
          <button 
            onClick={() => this.props.search_txt(this.state.nm)} 
            type="button" className="btn btn-outline-success btn-sm lft"
          >
            <i className="icon-ok"></i>
          </button>
        </div>  

      case 3:
        return <div className="find lft">
          <button 
            onClick={() => this.setState({ mode:0 })} 
            type="button" className="btn btn-outline-danger btn-sm lft"
          >
            <i className="icon-cancel"></i>
          </button>
          <div className="find-num">
            {
              this.state.nums.map((e,i) => 
                <input 
                  className={'form-control form-control-sm'+([2,5,7].includes(i) ? ' dv' : '')}
                  type="text" maxLength="1" 
                  key={i}
                  value={e}
                  onChange={
                    (e) => {
                      if(/^\d+$/.test(e.target.value))
                        this.setState({ nums: this.state.nums.map((el,ind) => ind != i ? el : e.target.value ) })
                    }
                  }
                />
              )
            }
          </div>
          <button 
            onClick={() => this.props.search_num(this.state.nums)} 
            type="button" className="btn btn-outline-success btn-sm lft"
          >
            <i className="icon-ok"></i>
          </button>
        </div>  
    }
  }
}

const mapDispatchToProps = (dispatch) => {
	return {
		search_txt: (nm) => {
      nm = nm.trim()
      if(nm!='')
        dispatch({ type: cmd.search_txt, payload: { nm } })
    },
    search_num: (nums) => {
      if(nums.join('')!='')
        dispatch({ type: cmd.search_num, payload: { nums } })
    }
	}
}

export default connect(null,mapDispatchToProps)(Search)
