import React from 'react'
import { connect } from 'react-redux'

const cmd = require('../commands')

class BookMarks extends React.Component {

  render() {
    return <div className="btn-abc">
      <button 
        onClick={() => this.props.select('%')} 
        type="button" className="btn btn-outline-dark btn-sm">
        &nbsp;&nbsp;*&nbsp;&nbsp;
      </button>
      {
        this.props.list.map(
          (e,i) => <button 
            onClick={() => this.props.select(e)} 
            key={i} 
            type="button" className="btn btn-outline-dark btn-sm">
            {e}
          </button>
        )
      }
    </div>
  }
}

const mapStateToProps = (state) => {
	return {
    list: state.bookmarks
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		select: (bookmark) => dispatch({ type: cmd.select, payload: { bookmark } })
	}
}

export default connect(mapStateToProps,mapDispatchToProps)(BookMarks)
