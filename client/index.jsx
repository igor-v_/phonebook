import React from 'react'
import { render } from 'react-dom'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import axios from 'axios'

import App from './components/App.jsx'

const 
  cmd = require('./commands'),
  mode = require('./modes'),
  
  appData = {
    mode: mode.query,
    list: [],
    bookmarks: [],
    curItem: {},
    err: ''
  }

const reducer = (state=appData, action) => {
  // console.log(action.type, action.payload)

  switch (action.type) {

    case cmd.add:
      return { ...state, mode: mode.edit, curItem: { id:0, nm:'', pn:'', ava:'' }, err: '' }

    case cmd.edit:
      return { ...state, mode: mode.edit, curItem: { ...action.payload.item }, err: '' }

    case cmd.cancel:
      return { ...state, mode: mode.list }

    case cmd.search_num:
      axios.post('search_num', action.payload)
        .then((resp) => store.dispatch({type: cmd.select_ok, payload: resp.data}))
        .catch((err) => alert(err))
      return { ...state, mode: mode.query }

    case cmd.search_txt:
      axios.post('search_txt', action.payload)
        .then((resp) => store.dispatch({type: cmd.select_ok, payload: resp.data}))
        .catch((err) => alert(err))
      return { ...state, mode: mode.query }

    case cmd.del:
      axios.delete(state.curItem.id)
        .then((resp) => store.dispatch({type: cmd.loaded, payload: resp.data}))
        .catch((err) => alert(err))
      return { ...state, mode: mode.query }

    case cmd.save:
      axios.post('save', action.payload)
        .then((resp) => store.dispatch({type: cmd.after_save, payload: resp.data}))
        .catch((err) => alert(err))
      return { ...state, mode: mode.query, curItem: {...action.payload} }

    case cmd.after_save:
      return action.payload.status=='Ok' ?
        {
          ...state,
          mode: mode.list,
          list: action.payload.list,
          bookmarks: action.payload.bookmarks
        }
        :
        {
          ...state, 
          mode: mode.edit,
          err: action.payload.msg
        }

    case cmd.select_ok:
      return { ...state, mode: mode.list, list: action.payload }

    case cmd.select:
      axios.post('bookmark', action.payload)
        .then((resp) => store.dispatch({type: cmd.select_ok, payload: resp.data}))
        .catch((err) => alert(err))
      return { ...state, mode: mode.query }

    case cmd.loaded:
      return {
        ...state,
        mode: mode.list,
        list: action.payload.list,
        bookmarks: action.payload.bookmarks
      }
  
    default:
      if(action.type.substr(0,12)==='@@redux/INIT'){
        axios.get('list')
          .then((resp) => store.dispatch({type: cmd.loaded, payload: resp.data}))
          .catch((err) => alert(err))
      } 
  }
  return state
}

const store = createStore(reducer, appData)

render(
	<Provider store={store}>
		<App />
	</Provider>,
	document.getElementById('root')
)
