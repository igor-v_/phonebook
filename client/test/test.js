(function() {
  module.exports = {
    bookmarks: ['А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ж', 'З'],
    list: [
      {
        id: 1,
        nm: 'Игорь',
        pn: '1234567890',
        ava: 'http://localhost:8080/test/av3.jpg'
      },
      {
        id: 2,
        nm: 'Елена',
        pn: '0523456123',
        ava: 'http://localhost:8080/test/av1.png'
      },
      {
        id: 3,
        nm: 'Владимир',
        pn: '0013456124',
        ava: ''
      },
      {
        id: 4,
        nm: 'Петр',
        pn: '3423456136',
        ava: 'http://localhost:8080/test/av0.jpg'
      },
      {
        id: 5,
        nm: 'Иван',
        pn: '7802356234',
        ava: ''
      },
      {
        id: 6,
        nm: 'Анатолий',
        pn: '0983456134',
        ava: 'http://localhost:8080/test/av2.jpg'
      },
      {
        id: 7,
        nm: 'Андрей',
        pn: '0345356243',
        ava: ''
      },
      {
        id: 8,
        nm: 'Сергей',
        pn: '0982352348',
        ava: ''
      },
      {
        id: 10,
        nm: 'Игорь',
        pn: '1304567890',
        ava: 'http://localhost:8080/test/av3.jpg'
      },
      {
        id: 11,
        nm: 'Игорь',
        pn: '2314567890',
        ava: 'http://localhost:8080/test/av3.jpg'
      },
      {
        id: 12,
        nm: 'Игорь',
        pn: '3324567890',
        ava: 'http://localhost:8080/test/av3.jpg'
      },
      {
        id: 13,
        nm: 'Игорь',
        pn: '4334567890',
        ava: 'http://localhost:8080/test/av3.jpg'
      },
      {
        id: 14,
        nm: 'Игорь',
        pn: '5344567890',
        ava: 'http://localhost:8080/test/av3.jpg'
      },
      {
        id: 15,
        nm: 'Игорь',
        pn: '6354567890',
        ava: 'http://localhost:8080/test/av3.jpg'
      },
      {
        id: 16,
        nm: 'Игорь',
        pn: '7364567890',
        ava: 'http://localhost:8080/test/av3.jpg'
      },
      {
        id: 17,
        nm: 'Игорь',
        pn: '8374567890',
        ava: 'http://localhost:8080/test/av3.jpg'
      },
      {
        id: 18,
        nm: 'Игорь',
        pn: '9384567890',
        ava: 'http://localhost:8080/test/av3.jpg'
      },
      {
        id: 19,
        nm: 'Игорь',
        pn: '0394567890',
        ava: 'http://localhost:8080/test/av3.jpg'
      },
      {
        id: 20,
        nm: 'Игорь',
        pn: '3104567890',
        ava: 'http://localhost:8080/test/av3.jpg'
      },
      {
        id: 21,
        nm: 'Игорь',
        pn: '3245671890',
        ava: 'http://localhost:8080/test/av3.jpg'
      },
      {
        id: 22,
        nm: 'Игорь',
        pn: '3345672890',
        ava: 'http://localhost:8080/test/av3.jpg'
      },
      {
        id: 23,
        nm: 'Игорь',
        pn: '3445673890',
        ava: 'http://localhost:8080/test/av3.jpg'
      },
      {
        id: 24,
        nm: 'Игорь',
        pn: '3545674890',
        ava: 'http://localhost:8080/test/av3.jpg'
      },
      {
        id: 25,
        nm: 'Игорь',
        pn: '3645675890',
        ava: 'http://localhost:8080/test/av3.jpg'
      },
      {
        id: 26,
        nm: 'Игорь',
        pn: '3745676890',
        ava: 'http://localhost:8080/test/av3.jpg'
      },
      {
        id: 27,
        nm: 'Игорь',
        pn: '3845677890',
        ava: 'http://localhost:8080/test/av3.jpg'
      },
      {
        id: 28,
        nm: 'Игорь',
        pn: '3945678890',
        ava: 'http://localhost:8080/test/av3.jpg'
      },
      {
        id: 29,
        nm: 'Игорь',
        pn: '3045679890',
        ava: 'http://localhost:8080/test/av3.jpg'
      },
      {
        id: 30,
        nm: 'Игорь',
        pn: '3451670890',
        ava: 'http://localhost:8080/test/av3.jpg'
      },
      {
        id: 31,
        nm: 'Игорь',
        pn: '3452678910',
        ava: 'http://localhost:8080/test/av3.jpg'
      },
      {
        id: 32,
        nm: 'Игорь',
        pn: '3453678920',
        ava: 'http://localhost:8080/test/av3.jpg'
      },
      {
        id: 33,
        nm: 'Игорь',
        pn: '3454678930',
        ava: 'http://localhost:8080/test/av3.jpg'
      },
      {
        id: 34,
        nm: 'Игорь',
        pn: '3455678940',
        ava: 'http://localhost:8080/test/av3.jpg'
      },
      {
        id: 35,
        nm: 'Игорь',
        pn: '3456678950',
        ava: 'http://localhost:8080/test/av3.jpg'
      },
      {
        id: 36,
        nm: 'Игорь',
        pn: '3457678960',
        ava: 'http://localhost:8080/test/av3.jpg'
      },
      {
        id: 37,
        nm: 'Игорь',
        pn: '3458678970',
        ava: 'http://localhost:8080/test/av3.jpg'
      },
      {
        id: 38,
        nm: 'Игорь',
        pn: '3459678980',
        ava: 'http://localhost:8080/test/av3.jpg'
      },
      {
        id: 39,
        nm: 'Игорь',
        pn: '3450678990',
        ava: 'http://localhost:8080/test/av3.jpg'
      },
      {
        id: 40,
        nm: 'Игорь',
        pn: '3456758900',
        ava: 'http://localhost:8080/test/av3.jpg'
      },
      {
        id: 41,
        nm: 'Игорь',
        pn: '3456768190',
        ava: 'http://localhost:8080/test/av3.jpg'
      },
      {
        id: 42,
        nm: 'Игорь',
        pn: '3456778290',
        ava: 'http://localhost:8080/test/av3.jpg'
      }
    ]
  };

}).call(this);


//# sourceMappingURL=test.js.map
//# sourceURL=coffeescript