module.exports = {
  entry: './index.jsx',
  output: {
    path: '/home/user/phonebook/server/public', 
    filename: 'index.js'    
  },    
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      }
    ]
  },
  devServer: {
    contentBase: '/home/user/phonebook/server/public', 
    compress: true,
    port: 8080
  }
};

