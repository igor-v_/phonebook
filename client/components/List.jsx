import React from 'react'
import { connect } from 'react-redux'

import Search from './Search.jsx'
import BookMarks from './BookMarks.jsx'
import Cards from './Cards.jsx'

const cmd = require('../commands')

class List extends React.Component {
  render() {
    return <div className="container">
      <div className="row">
        <div className="col">
          <button onClick={() => this.props.add()} type="button" className="btn btn-outline-primary btn-sm lft">
            <i className="icon-add"></i>
          </button>
          <Search />
          <BookMarks />
        </div>
      </div>
      <Cards />
    </div>
  }
}

const mapDispatchToProps = (dispatch) => {
	return {
		add: () => dispatch({ type: cmd.add })
	}
}

export default connect(null,mapDispatchToProps)(List)
