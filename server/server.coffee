# Если деплоить под апач+пассенджер:
# app.listen 'passenger', () -> console.log 'Ok - passenger'
#
# и как подкаталог-приложение, например, http://domain.name/pb
# то указать '/pb' - в переменной 'appPath'

appPath = ''
# appPath = '/pb'

express = require 'express'
bodyParser = require 'body-parser'
sqlite = require 'sqlite3'

db = new sqlite.Database './PhoneBook.db', sqlite.OPEN_READWRITE

getList = (res, bm = '') ->
  db.all 'select * from v_nm', (err, rows) ->
    bookmarks = rows.map (e) -> e.nm1
    bm_ = if bm is '' then bookmarks[0] else bm
    db.all 'select * from phone where nm like ? order by nm asc', bm_+'%', (err, rows) ->
      res.json { status: 'Ok', bookmarks: bookmarks, list: rows }

errTxt = (str) ->
  if str.includes 'phone.nm'
    'Такое имя уже есть ...'
  else if str.includes 'phone.pn'
    'Такой номер уже есть ...'
  else
    str

afterSave = (req, res, err) ->
  if err
    res.json { status: 'Err', msg: errTxt(err.message) } 
  else 
    getList res, req.body.nm.charAt(0)

app = express()
app.use "#{appPath}/", express.static 'public'
app.use bodyParser.json()

app.get "#{appPath}/list", (req, res) -> getList res

app.post "#{appPath}/bookmark", (req, res) ->
  db.all 'select * from phone where nm like ? order by nm asc', req.body.bookmark+'%', (err, rows) ->
    res.json rows

app.post "#{appPath}/save", (req, res) ->
  if +req.body.id==0
    db.run 'insert into phone (nm, pn, ava) values (?, ?, ?)', req.body.nm, req.body.pn, req.body.ava, (err) ->
      afterSave req, res, err
  else
    db.run 'update phone set nm=?, pn=?, ava=? where id=?', req.body.nm, req.body.pn, req.body.ava, req.body.id, (err) ->
      afterSave req, res, err

app.delete "#{appPath}/:id", (req, res) ->
  db.run 'delete from phone where id=?', req.params.id, (err) -> getList res

app.post "#{appPath}/search_txt", (req, res) ->
  db.all 'select * from phone where nm like ? order by nm asc', '%'+req.body.nm+'%', (err, rows) ->
    res.json rows

app.post "#{appPath}/search_num", (req, res) ->
  where = []  
  req.body.nums.forEach (e,i) -> 
    where.push "substr(pn,#{i+1},1)='#{e}'" if e isnt ''

  db.all "select * from phone where #{where.join(' and ')} order by nm asc", (err, rows) ->
    res.json rows

app.listen 8080, () -> console.log "Ok - http://localhost:8080 ...\nCtrl-C - break"
# app.listen 'passenger', () -> console.log 'Ok - passenger'