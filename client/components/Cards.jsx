import React from 'react'
import { connect } from 'react-redux'

const cmd = require('../commands')

class Cards extends React.Component {

  updateHeight() {
    document.getElementsByClassName('vscroll')[0].style.height = (window.innerHeight-40)+'px'
  }

  componentDidMount() {
    this.updateHeight()
    window.addEventListener("resize", this.updateHeight.bind(this))
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateHeight.bind(this))
  }

  render() {
    return <div className="row">
      <div className="col vscroll">
        {
          this.props.list.map((e) => {
            let img = e.ava=='' ? null : <img src={e.ava} />
            return <div key={e.id} className="card">
              <div className="ava">
                {img}
                <button 
                  onClick={() => this.props.edit(e) } 
                  type="button" className="btn btn-outline-danger btn-sm lft bt-edt"
                >
                  <i className="icon-edit"></i>
                </button>
              </div>
              <h1>{e.nm}</h1>
              <h2>{e.pn.slice(0,3)+' '+e.pn.slice(3,6)+' '+e.pn.slice(6,8)+' '+e.pn.slice(8,10)}</h2>
              <div className="clr"></div>
            </div>
          })
        }
      </div>
    </div>
  }
}

const mapStateToProps = (state) => {
	return {
    list: state.list
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		edit: (item) => dispatch({ type: cmd.edit, payload: { item } })
	}
}

export default connect(mapStateToProps,mapDispatchToProps)(Cards)
