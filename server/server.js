(function() {
  // Если деплоить под апач+пассенджер:
  // app.listen 'passenger', () -> console.log 'Ok - passenger'

  // и как подкаталог-приложение, например, http://domain.name/pb
  // то указать '/pb' - в переменной 'appPath'
  var afterSave, app, appPath, bodyParser, db, errTxt, express, getList, sqlite;

  appPath = '';

  // appPath = '/pb'
  express = require('express');

  bodyParser = require('body-parser');

  sqlite = require('sqlite3');

  db = new sqlite.Database('./PhoneBook.db', sqlite.OPEN_READWRITE);

  getList = function(res, bm = '') {
    return db.all('select * from v_nm', function(err, rows) {
      var bm_, bookmarks;
      bookmarks = rows.map(function(e) {
        return e.nm1;
      });
      bm_ = bm === '' ? bookmarks[0] : bm;
      return db.all('select * from phone where nm like ? order by nm asc', bm_ + '%', function(err, rows) {
        return res.json({
          status: 'Ok',
          bookmarks: bookmarks,
          list: rows
        });
      });
    });
  };

  errTxt = function(str) {
    if (str.includes('phone.nm')) {
      return 'Такое имя уже есть ...';
    } else if (str.includes('phone.pn')) {
      return 'Такой номер уже есть ...';
    } else {
      return str;
    }
  };

  afterSave = function(req, res, err) {
    if (err) {
      return res.json({
        status: 'Err',
        msg: errTxt(err.message)
      });
    } else {
      return getList(res, req.body.nm.charAt(0));
    }
  };

  app = express();

  app.use(`${appPath}/`, express.static('public'));

  app.use(bodyParser.json());

  app.get(`${appPath}/list`, function(req, res) {
    return getList(res);
  });

  app.post(`${appPath}/bookmark`, function(req, res) {
    return db.all('select * from phone where nm like ? order by nm asc', req.body.bookmark + '%', function(err, rows) {
      return res.json(rows);
    });
  });

  app.post(`${appPath}/save`, function(req, res) {
    if (+req.body.id === 0) {
      return db.run('insert into phone (nm, pn, ava) values (?, ?, ?)', req.body.nm, req.body.pn, req.body.ava, function(err) {
        return afterSave(req, res, err);
      });
    } else {
      return db.run('update phone set nm=?, pn=?, ava=? where id=?', req.body.nm, req.body.pn, req.body.ava, req.body.id, function(err) {
        return afterSave(req, res, err);
      });
    }
  });

  app.delete(`${appPath}/:id`, function(req, res) {
    return db.run('delete from phone where id=?', req.params.id, function(err) {
      return getList(res);
    });
  });

  app.post(`${appPath}/search_txt`, function(req, res) {
    return db.all('select * from phone where nm like ? order by nm asc', '%' + req.body.nm + '%', function(err, rows) {
      return res.json(rows);
    });
  });

  app.post(`${appPath}/search_num`, function(req, res) {
    var where;
    where = [];
    req.body.nums.forEach(function(e, i) {
      if (e !== '') {
        return where.push(`substr(pn,${i + 1},1)='${e}'`);
      }
    });
    return db.all(`select * from phone where ${where.join(' and ')} order by nm asc`, function(err, rows) {
      return res.json(rows);
    });
  });

  app.listen(8080, function() {
    return console.log("Ok - http://localhost:8080 ...\nCtrl-C - break");
  });

  // app.listen 'passenger', () -> console.log 'Ok - passenger'

}).call(this);


//# sourceMappingURL=server.js.map
//# sourceURL=coffeescript