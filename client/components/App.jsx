import React from 'react'
import { connect } from 'react-redux'

import List from './List.jsx'
import Edit from './Edit.jsx'

const mode = require('../modes')

class App extends React.Component {
  render() {
    switch (this.props.mode) {
      case mode.query: 
        return <div className="container">
          <div className="row">
            <div className="col cntr"><img src="img/wait.gif" /></div>
          </div>
        </div>
      case mode.list: return <List />
      case mode.edit: return <Edit />
    }
  }
}

const mapStateToProps = (state) => {
	return {
		mode: state.mode
	}
}

export default connect(mapStateToProps)(App)
