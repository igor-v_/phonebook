(function() {
  module.exports = {
    add: 'ADD',
    edit: 'EDIT',
    save: 'SAVE',
    after_save: 'AFTER_SAVE',
    del: 'DEL',
    loaded: 'LOADED',
    cancel: 'CANCEL',
    select: 'SELECT',
    select_ok: 'SELECT_OK',
    search_txt: 'SEARCH_TXT',
    search_num: 'SEARCH_NUM'
  };

}).call(this);


//# sourceMappingURL=commands.js.map
//# sourceURL=coffeescript