import React from 'react'
import { connect } from 'react-redux'

const cmd = require('../commands')

class Edit extends React.Component {
  constructor(props) {
    super(props)
    this.state = { ...props.item, err: props.err }
  }

  render() {
    let btDel = +this.state.id!=0 ?
      <button onClick={() => this.props.del(this.state)} type="button" className="btn btn-outline-danger btn-sm rht">
        <i className="icon-del"></i>
      </button>
      :
      null

    let divErr = this.state.err!='' ?
      <div className="alert alert-danger" role="alert">{this.state.err}</div>
      :
      null
      
    return <div className="container">
      <div className="row justify-content-center">
        <div className="col-md-6">
          &nbsp;
          <input 
            onChange={(e) => this.setState({ava: e.target.value})} 
            value={this.state.ava} 
            className="form-control form-control" 
            type="text" 
            placeholder="URL аватарки ..." 
          />
          &nbsp;
          <div className="form-row">
            <div className="col-3">
              <img className="ava-edit" src={this.state.ava} />
            </div>
            <div className="col-9">
              <input 
                onChange={
                  (e) => {
                    if(/^[А-Яа-яІіЄєЇї ]+$/.test(e.target.value) || e.target.value=='')
                      this.setState({nm: e.target.value})
                  }
                }
                value={this.state.nm} 
                className="form-control form-control" 
                type="text" 
                placeholder="имя ..." 
                maxLength="70" 
              />
              &nbsp;
              <input 
                onChange={
                  (e) => {
                    if(/^\d+$/.test(e.target.value) || e.target.value=='')
                      this.setState({pn: e.target.value})
                  }
                }
                value={this.state.pn} 
                className="form-control form-control" 
                type="text" 
                placeholder="номер ..." 
                maxLength="10" 
              />
              &nbsp;
            </div>
          </div>

          <div className="form-row">
            <div className="col">
              <button 
                onClick={() => {
                  let
                    nm = this.state.nm.trim(),
                    pn = this.state.pn.trim()
                  
                  if(nm=='') this.setState({ err: 'Имя должно быть ...' })
                  else if(pn=='' || pn.length<10) this.setState({ err: 'Номер должен быть 10 цифр ...' })
                  else this.props.save(this.state)
                }} 
                type="button" className="btn btn-outline-success btn-sm"
              >
                <i className="icon-ok"></i>
              </button>
            </div>
            <div className="col">
              {btDel}
            </div>
            <div className="col">
              <button onClick={() => this.props.cancel()} type="button" className="btn btn-outline-dark btn-sm rht">
                <i className="icon-cancel"></i>
              </button>
            </div>
          </div>
          
          {divErr}

        </div>
      </div>
    </div>  
  }
}

const mapStateToProps = (state) => {
	return {
    item: state.curItem,
    err: state.err
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		cancel: () => dispatch({ type: cmd.cancel }),
		del: (item) => {
      if(confirm(`Удалить ${item.nm} ?`)) 
        dispatch({ type: cmd.del })
    },
    save: (item) => {
      item.nm = item.nm.trim()
      item.nm = item.nm.charAt(0).toUpperCase() + item.nm.slice(1)
      dispatch({ type: cmd.save, payload: item })
    }
	}
}

export default connect(mapStateToProps,mapDispatchToProps)(Edit)
